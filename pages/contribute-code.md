title=Contributing code to Adblock Plus


## {{things-to-work-on Finding something to work on}}

* {{javascript-work If you know **JavaScript**, you can work on the [Adblock Plus core code](modules#core), the [Adblock Plus Web Extension](modules#platform) or [our websites](modules#websites).}}
* {{python-work If you know **Python**, you can work on our [backend](modules#sitescripts).}}
* {{cpp-work If you know **C++**, you can work on [Adblock Plus for Internet Explorer](modules#adblock-plus-for-internet-explorer).}}
* {{java-work If you know **Java**, you can work on [Adblock Browser for Android](modules#adblock-browser-for-android) or [Adblock Plus for Samsung Browser](modules#adblock-plus-for-samsung-browser).}}
* {{swift-work If you know **Swift**, you can work on [Adblock Plus for iOS/macOS](modules#adblock-plus-for-ios)}}
* {{puppet-work If you know **Puppet**, you can work on [our infrastructure](modules#infrastructure).}}

{{issues We're keeping track of all the things we want to fix and improve in the issue tracker of our [projects on GitLab](https://gitlab.com/eyeo/adblockplus).}}

## {{getting-in-touch Getting in touch}}
{{irc Need help or want to chat? Join our IRC channel: [#adblockplus on irc.mozilla.org](irc://irc.mozilla.org/#adblockplus) (there is also a [web-based client](http://mibbit.com/?server=irc.mozilla.org&amp;channel=%23adblockplus)). While we are a globally distributed team, there are times when most of the team is not online. Please note that we cannot reply to you if you are offline.}}

## {{submitting-a-patch Submitting a patch}}

{{coding-style Once you've made and tested your changes, first make sure they're in line with our [coding style](coding-style).}}

{{submit-merge-request Now you can [create a merge request](https://docs.gitlab.com/ce/gitlab-basics/add-merge-request.html) in the respective project on GitLab.}}

{{contributor-agreement Finally, before we can accept your patch, you need to sign the [contributor agreement](https://adblockplus.org/eyeo-contributor-license-agreement.pdf). Please print it out, sign it, scan that or take a picture of it and send it to one of your reviewers.}}

## {{becoming-a-comitter Becoming a committer}}

{{becoming-a-comitter If you intend to contribute to a project on a regular basis, you might want to become a [committer](committer).}}
